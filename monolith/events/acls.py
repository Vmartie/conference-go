from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    url = f"https://api.pexels.com/v1/search?query={city}&{state}?per_page=1"
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, headers=headers)
    photo = json.loads(response.content)
    photo_url = photo["photos"][0]["url"]

    return photo_url


def get_weather_data(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&appid={OPEN_WEATHER_API_KEY}"
    headers = {"Authorization": OPEN_WEATHER_API_KEY}
    response = requests.get(url, headers=headers)
    geo_coordinates = json.loads(response.content)
    lon = geo_coordinates[0]["lon"]
    lat = geo_coordinates[0]["lat"]

    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    headers = {"Authorization": OPEN_WEATHER_API_KEY}
    response = requests.get(url, headers=headers)
    weather_data = json.loads(response.content)
    weather_dict = {
        "temp": weather_data["main"]["temp"],
        "description": weather_data["weather"][0]["description"],
    }
    return weather_dict
